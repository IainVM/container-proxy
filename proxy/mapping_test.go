package proxy_test

import (
	"testing"

	"gitlab.com/iainvm/container-proxy/proxy"
)

func TestNewMappings(t *testing.T) {
	proxy := proxy.NewMappings()

	if len(proxy.Destinations()) != 0 {
		t.Fail()
	}
}

func TestAddDestination(t *testing.T) {

	server := proxy.NewMappings()

	// Given

	name := "test-name"
	var port uint16 = 1234
	pathSegment := "path-seg"
	destination := proxy.NewDestination(name, port)

	server.AddDestination(pathSegment, destination)

	if server.Destinations()[pathSegment] != destination {
		t.Fail()
	}

	if server.GetDestination(pathSegment) != destination {
		t.Fail()
	}
}
