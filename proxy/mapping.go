package proxy

import (
	"github.com/docker/docker/api/types"
)

type Mappings struct {
	destinations map[string]*Destination
}

func NewMappings() *Mappings {
	proxy := &Mappings{}

	// Don't know length needed can't preinitialise
	proxy.destinations = make(map[string]*Destination)
	return proxy
}

func (proxy *Mappings) GetDestination(pathSegment string) *Destination {
	val, exists := proxy.destinations[pathSegment]

	if exists {
		return val
	}
	return nil
}

func (proxy *Mappings) Destinations() map[string]*Destination {
	return proxy.destinations
}

func (proxy *Mappings) AddDestination(pathSegment string, destination *Destination) {
	proxy.destinations[pathSegment] = destination
}

func (proxy *Mappings) AddContainer(dockerContainer types.Container) {

	containerName := dockerContainer.Names[0][1:]

	pathSegment := containerName
	name := containerName
	var port uint16 = 1234
	destination := NewDestination(name, port)
	proxy.AddDestination(pathSegment, destination)
}
