package proxy

type Destination struct {
	name string
	port uint16
}

func NewDestination(name string, port uint16) *Destination {

	destinations := &Destination{}

	destinations.name = name
	destinations.port = port

	return destinations
}
