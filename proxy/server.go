package proxy

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/docker/docker/api/types"
)

type Server struct {
	server   *http.Server
	mappings *Mappings
}

func NewServer(port uint16) *Server {

	server := &Server{
		server:   nil,
		mappings: NewMappings(),
	}

	server.server = &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%d", port),
		Handler: http.HandlerFunc(server.handle),
	}

	return server
}

func (server *Server) AddContainer(container types.Container) {
	server.mappings.AddContainer(container)
}

func (server *Server) Listen() {
	server.server.ListenAndServe()
}

func (server *Server) handle(w http.ResponseWriter, r *http.Request) {

	urlPart := strings.Split(r.URL.Path[1:], "/")
	fmt.Printf("urlPart: %v\n", urlPart)
	if r.URL.Path == "/" {
		mappings := server.mappings.destinations

		message := ""
		for pathSeg, mapping := range mappings {
			message += fmt.Sprintf("Path: %v\nName: %v\nPort: %v\n\n", pathSeg, mapping.name, mapping.port)
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(message))
	} else {
		pathSeg := urlPart[0]
		fmt.Printf("pathSeg: %v\n", pathSeg)
		mapping := server.mappings.GetDestination(pathSeg)

		if mapping == nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Not Found"))
		} else {
			message := fmt.Sprintf("mapping.name: %v\nmapping.port: %v\n", mapping.name, mapping.port)

			w.WriteHeader(http.StatusOK)
			w.Write([]byte(message))
		}

	}

}
