package proxy

import "testing"

func TestNewDestination(t *testing.T) {

	name := "test-name"
	var port uint16 = 1234

	destination := NewDestination(name, port)

	if destination.name != name {
		t.Fail()
	}

	if destination.port != port {
		t.Fail()
	}
}
