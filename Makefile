run:
	go run ./cmd/container-proxy/

test:
	go test -v -cover ./...
