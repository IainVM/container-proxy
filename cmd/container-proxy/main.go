package main

import (
	"context"
	"fmt"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/events"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"gitlab.com/iainvm/container-proxy/proxy"
)

func main() {
	ctx := context.Background()

	server := proxy.NewServer(uint16(3000))

	docker, _ := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())

	go watch(ctx, docker, server)

	processCurrentContainers(ctx, docker, server)
	server.Listen()
}

func watch(ctx context.Context, docker *client.Client, server *proxy.Server) {
	events, errs := docker.Events(ctx, types.EventsOptions{})

	for {
		select {
		case err := <-errs:
			panic(err)
		case event := <-events:

			if event.Type == "container" && event.Action == "start" {
				container, _ := parseEventForContainer(ctx, docker, event)
				fmt.Println("Found new started container: ", container.Names[0][1:])
				server.AddContainer(container)
			}
		}
	}
}

func parseEventForContainer(ctx context.Context, cli *client.Client, event events.Message) (types.Container, types.ContainerJSON) {
	container_id := event.ID

	list_filters := filters.NewArgs(
		filters.Arg("id", container_id),
	)

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{Filters: list_filters})
	if err != nil {
		panic(err)
	}

	if len(containers) > 0 {
		container := containers[0]
		container_json, err := cli.ContainerInspect(ctx, container.ID)
		if err != nil {
			panic(err)
		}

		return container, container_json
	}

	panic("I don't know")
}

func processCurrentContainers(ctx context.Context, docker *client.Client, server *proxy.Server) {
	containers, err := docker.ContainerList(ctx, types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}

	for _, container := range containers {
		server.AddContainer(container)
	}

}
